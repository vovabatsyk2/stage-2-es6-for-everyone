import { controls } from '../../constants/controls';

function healthBar(selector, fullHealth, health) {
  const bar = document.querySelector(selector);
  if (bar.style.width === '') {
    bar.style.width = '100%';
  }
  const res = health * 100 / fullHealth;
  bar.style.width = res + '%';
}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstFighterHealth = firstFighter.health;
    const secondFighterHealth = secondFighter.health;
    let isFirstFighterBlocked = false;
    let isSecondFighterBlocked = false;
    let isFirstPlayerCriticalHit = true;
    let isSecondPlayerCriticalHit = true;

    window.addEventListener('keyup', (e) => {
      switch (e.code) {
        case controls.PlayerOneAttack:
          if (secondFighter.health > 0 && !isSecondFighterBlocked && !isFirstFighterBlocked) {
            secondFighter.health = secondFighter.health - getDamage(firstFighter, secondFighter);
            healthBar('#right-fighter-indicator', secondFighterHealth, secondFighter.health);
          }
          break;

        case controls.PlayerOneBlock:
          isFirstFighterBlocked = !isFirstFighterBlocked;
          break;

        case controls.PlayerOneCriticalHitCombination:
          if (isFirstPlayerCriticalHit) {
            isFirstPlayerCriticalHit = false;
            if (secondFighter.health >= 0 & !isFirstFighterBlocked) {
              secondFighter.health = secondFighter.health - firstFighter.attack * 2;
              healthBar('#right-fighter-indicator', secondFighterHealth, secondFighter.health);
            }
            new Promise(resolve => {
              setTimeout(() => {
                isFirstPlayerCriticalHit = true;
                return resolve(isFirstPlayerCriticalHit);
              }, 10000);
            });
          }
          break;

        case controls.PlayerTwoAttack:
          if (firstFighter.health > 0 && !isFirstFighterBlocked && !isSecondFighterBlocked) {
            firstFighter.health = firstFighter.health - getDamage(secondFighter, firstFighter);
            healthBar('#left-fighter-indicator', firstFighterHealth, firstFighter.health);
          }
          break;

        case controls.PlayerTwoCriticalHitCombination:
          if (isSecondPlayerCriticalHit) {
            isSecondPlayerCriticalHit = false;
            if (firstFighter.health >= 0 & !isSecondFighterBlocked) {
              firstFighter.health = firstFighter.health - secondFighter.attack * 2;
              healthBar('#left-fighter-indicator', firstFighterHealth, firstFighter.health);
            }
            new Promise(resolve => {
              setTimeout(() => {
                isSecondPlayerCriticalHit = true;
                return resolve(isSecondPlayerCriticalHit);
              }, 10000);
            });
          }
          break;

        case controls.PlayerTwoBlock:
          isSecondFighterBlocked = !isSecondFighterBlocked;
          break;
      }
      if (secondFighter.health <= 0) return resolve(firstFighter);
      if (firstFighter.health <= 0) return resolve(secondFighter);
    });
  });
}

export function getDamage(attacker, defender) {
  if (getHitPower(attacker) < getBlockPower(defender)) return 0;
  else return Math.abs(getHitPower(attacker) - getBlockPower(defender));
}

export const getHitPower = (fighter) => fighter.attack * getRandomFloat(1, 2);
export const getBlockPower = (fighter) => fighter.defense * getRandomFloat(1, 2);

const getRandomFloat = (min, max) => Math.random() * (max - min) + min;
